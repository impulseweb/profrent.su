<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

if (!Request::is('admin')){

	/* Route Catalog/Products */
	Route::get('shop', 'App\Http\Controllers\CatalogController@Shop');

	Route::get('shop/categories', 'App\Http\Controllers\CatalogController@Categories');
	Route::get('shop/categories/{category}', 'App\Http\Controllers\CatalogController@Category');

	Route::get('shop/products', 'App\Http\Controllers\CatalogController@Products');
	Route::get('shop/products/{product}', 'App\Http\Controllers\CatalogController@Product');


	/* Route Pages */
	Route::get('{slug}', 'App\Http\Controllers\PageController@Pages');

	/* Route Cart */
	Route::get('cart', 'App\Http\Controllers\CartController@Cart');

	/* Route Checkout */
	Route::get('checkout', 'App\Http\Controllers\CheckoutController@Checkout');
}


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
