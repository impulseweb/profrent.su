<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class CatalogController extends Controller{

	public function Shop(){

		/* Вывод всех категорий из БД */

		$categories = DB::select('select id,name,slug,parent_id from categories');
		$level_1 = array();
		$level_2 = array();

		/* Формирование 1 и 2 уровня категорий */

		foreach($categories as $category){
			if(empty($category->parent_id)){
				$level_1[$category->id]['id'] = $category->id;
				$level_1[$category->id]['name'] = $category->name;
				$level_1[$category->id]['link'] = $category->slug;
			}else{
				if(isset($level_1[$category->parent_id])){
					$level_1[$category->parent_id]['childrens'][$category->id]['id'] = $category->id;
					$level_1[$category->parent_id]['childrens'][$category->id]['name'] = $category->name;
					$level_1[$category->parent_id]['childrens'][$category->id]['link'] = $category->slug;
				}else{
					$level_2[$category->parent_id]['parent_id'] = $category->parent_id;
					$level_2[$category->parent_id]['id'] = $category->id;
					$level_2[$category->parent_id]['name'] = $category->name;
					$level_2[$category->parent_id]['link'] = $category->slug;

				}
			}
		}

		/* Формирование 3 уровня категорий */

		foreach($level_1 as $parent_idx => $category){
			if(isset($category['childrens'])){
				foreach($category['childrens'] as $idx => $childrens){
					if($idx == $level_2[$idx]['parent_id']){
						$level_1[$parent_idx]['childrens'][$idx]['childrens'] = $level_2[$idx];
					}
				}
			}
		}

		//print_R($level_1);


		/* Вывод всех товаров из БД */

		$products = DB::select('select id,title,slug,excerpt,image,price,specifications from posts WHERE status = "PUBLISHED"');

		//print_r($products);
	}

	public function Category($category){

		/* Сбор данных текущей категрии */

		$current = DB::select('select id,name,slug,content,image from categories WHERE slug = "' . $category . '"');

		if(!empty($current)){
			/* Сбор данных дочерних категорий */

			$idx = $current[0]->id;

			$categories = DB::select('select id,name,slug from categories WHERE parent_id = "' . $idx . '"');

			/* Сбор продуктов текущей категории */

			$products = DB::select('select id,title,slug,image,price from posts WHERE category_id = "' . $idx . '" AND status = "PUBLISHED"');
		}else{
			echo 404;
		}
	}

	public function Product($product){

		$data = array();

		$query = DB::select('select * from posts WHERE slug = "' . $product . '" AND status = "PUBLISHED"');

		if(!empty($query)){
			foreach($query as $item){
				$data['id'] = $item->id;
				$data['category_id'] = $item->category_id;
				$data['title'] = $item->title;
				$data['seo_title'] = $item->seo_title;
				$data['meta_description'] = $item->meta_description;
				$data['meta_keywords'] = $item->meta_keywords;
				$data['specifications'] = json_decode($item->specifications, true);
				$data['price'] = $item->price;
				$data['price_discount'] = $item->price_discount;
				$data['availability'] = json_decode($item->availability, true);
				$data['transportation'] = json_decode($item->transportation, true);
				$data['images'] = $item->images;
				$data['slug'] = $item->slug;
			}
			
			print_r($data);
			
		}else{
			echo 404;
		}

	}

}